
/*
    service/lib/genericService.js
*/

var config = require('../../config');
var repository = require('../../repository');
var validator = require('../../validator/');
var _ = require('underscore');

exports.Create = function(model_name, model_object){

    var _modelName = model_name;
    var _modelObject = model_object;

    // this.saveLog = function(method_name, erro){
    //     var log = new repository.logModel.LogModel();
    //     log.descricao = _modelName + ' - ' + method_name;
    //     log.modelo = null;
    //     log.erro = erro;
    //     log.data = new Date;
    //     log.save();
    // };

    // Retorna todos os documentos de uma Model, caso atenda a condição
    this.getBy = function(query, callback){
        var model = config.getConnection().model(_modelName, _modelObject);
        model.findOne(query, function(err, m){
            if(err){
                callback(null);
            }
            callback(m);
            return;
        });
    };

    // Retorna todos os documentos de uma Model
    this.getAll = function(callback){
        var model_list = config.getConnection().model(_modelName, _modelObject);
        model_list.find(function(err, m){
             if(err){
                callback(null);
            }
            callback(m);
            return;
         });
    };

    // Remove um documento, caso atenda a condição
    this.remove = function(query, callback){
        var retorno = new validator.validatorResult.Create();
        retorno.data = null;
        var model = config.getConnection().model(_modelName, _modelObject);
         model.remove(query, function(err){
             retorno.isValid = true;
             if(err){ 
                 retorno.isValid = false; 
             }
             callback(retorno);
             return;
        });
    };

    // Salvando documento
    this.save = function(model, callback){
        model.save(function(err, obj){
            if(err){
                callback(null);
            }

            callback(err, model);
            return;
        });
    };

    // Atualizando documento
	this.update = function(query, model, callback){
        var model_update = config.getConnection().model(_modelName, _modelObject);
        model_update.findOne(query, function(err, ret){
            if(err != null)
                callback(err);
                
            ret.Compare(model);
	        ret.save();
            callback(ret);
        });
    };
}



