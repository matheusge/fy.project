/*
    service/lib/produtoService.js
*/

var config = require('../../config');
var repository = require('../../repository');;
var validator = require('../../validator/');

exports.Create = function()
{
    this.save = function(produto, callback){

        var validatorProduto = new validator.produtoValidator.Create();
        var retorno = validatorProduto.validate(produto);

        if(!retorno.isValid){
            retorno.data = null;
            callback(retorno);
            return;
        }

        produto.save(function(err, obj){
            if(err){
                retorno.data = null;
                callback(retorno);
                return;
            }else{
                retorno.data = produto;
			    callback(retorno);
			    return;
            }
        });
    };

    this.update = function(id, p, callback){

        var validatorProduto = new validator.produtoValidator.Create();
        var retorno = validatorProduto.validate(p);

        if(!retorno.isValid){
            retorno.data = null;
            callback(retorno);
            return;
        }

        var produto = config.getConnection().model('Produto', repository.produtoModel.ProdutoModel);
        produto.findOne({ _id: id}, function(err, ret){
            if(err){
                retorno.data = null;
                callback(retorno)
            }else{
                ret.nome = p.nome;
                ret.save();

                retorno.data = ret;
                callback(retorno);
            }
        });
    };
}



