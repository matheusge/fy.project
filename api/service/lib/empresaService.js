/*
    service/lib/empresaService.js
*/

var config = require('../../config');
var repository = require('../../repository');
var validator = require('../../validator/');
var genericService = require('../../service/lib/genericService');

exports.Create = function(){
    //Herdando métodos genéricos - CREATE/READ/UPDATE/DELETE
    var _genericService = new genericService.Create('Empresa', repository.empresaModel.EmpresaModel);
    for(var n in _genericService) { this[n] = _genericService[n] };
};