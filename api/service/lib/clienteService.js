/*
    service/lib/clienteService.js
*/

var config = require('../../config');
var repository = require('../../repository');
var validator = require('../../validator/');
var genericService = require('../../service/lib/genericService');

exports.Create = function(){
    //Herdando métodos genéricos - CREATE/READ/UPDATE/DELETE
    var _genericService = new genericService.Create('Cliente', repository.clienteModel.ClienteModel);
    for(var n in _genericService) { this[n] = _genericService[n] };
};

    // this.save = function(cliente, callback){
    //     var validatorCliente = new validator.clienteValidator.Create();
    //     var retorno = validatorCliente.validate(cliente);
    //     if(!retorno.isValid){
    //         retorno.data = null;
    //         callback(retorno);
    //         return;
    //     }
    // };


