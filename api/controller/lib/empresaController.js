/*
    controller/lib/empresaController.js
*/

var dto = require('../../dto');
var service = require('../../service');
var repository = require('../../repository');
var validator = require('../../validator/');
var _ = require('underscore');

exports.Create = function(){

    this.getById = function(id, callback){
        var resdto = new dto.responseDto.Create();
        var serviceEmpresa = new service.empresaService.Create();
        var empresa = serviceEmpresa.getBy({ id : this.id }, function(emp){
            if(emp != null){
                resdto.resposta = "OK";
                resdto.data = emp;
            }else{
                resdto.resposta = "Nenhuma empresa encontrada";
                resdto.data = null;
            }
            callback(resdto);
        });
    }

    this.getAll = function(callback){
        var resdto = new dto.responseDto.Create();
        var serviceEmpresa = new service.empresaService.Create();
        var empresa = serviceEmpresa.getAll(function(emp){
            if(emp != null){
                resdto.resposta = "OK";
                resdto.data = emp;
            }else{
                aresdto.resposta = "Nenhuma empresa encontrada";
                resdto.data = null;
            }
            callback(resdto);
        });
    };

    this.remove = function(data, callback){
        var serviceEmpresa = new service.empresaService.Create();
        serviceEmpresa.remove({ _id: data }, function(ret){
            var a = new dto.responseDto.Create();
            if(!ret.isValid){
                a.responseDto = "Erro"
                a.data = ret.data;
            }
            else{
                a.resposta = "Excluído com sucesso!";
                a.data = ret.data;
            }
            callback(a);
        });
    };
    
    this.update = function(data, callback){
        var param = { id: data.id };
        var e = new repository.empresaModel.EmpresaModel();
        e.id = data._id;
        e.config_pontos = data.config_pontos;
        e.cnpj = data.cnpj;
        e.nome = data.nome;

        var validatorEmpresa = new validator.empresaValidator.Create();
        var retorno = validatorEmpresa.validate(e);

        if(!retorno.isValid)
        {
            retorno.data = null;
             _.each(retorno.mensagemValidacao, function(item){
	             retorno.mensagemValidacao.push(item);
             });   
             callback(retorno);
        }
        else
        {
            var serviceEmpresa = new service.empresaService.Create();
            serviceEmpresa.update(param, e, function(err, ret){
                var a = new dto.responseDto.Create();
                if(err != null){
                    a.resposta = "Objeto inválido";
                    a.data = ret;
	                a.mensagemValidacao.push(JSON.stringify(err));
                }
                else
                {
                    a.resposta = "OK";
                    a.data = ret;
                }
                callback(a)
            });
        };
    };

    this.create = function(data, callback){
        var u = new repository.usuarioModel.UsuarioModel();
        u.nome = data.nomeusu;
        u.email = data.emailusu;
        u.password = data.passusu;

        var e = new repository.empresaModel.EmpresaModel();
        e.nome = data.nome;
        e.cnpj = data.cnpj;
        e.config_pontos = data.config_pontos;
        e.usuarios.push(u);

        var validatorEmpresa = new validator.empresaValidator.Create();
        //var validatorUsuario = new validator.usuarioValidator.Create();

        var retorno = validatorEmpresa.validate(e);
        //var retorno_usuario = validatorUsuario.validate(u);
        if(!retorno.isValid) //|| !retorno.isValid)
        {
            retorno.data = null;
            
            //Adicionando as mensagens de validação do Usuário
            _.each(retorno.mensagemValidacao, function(item){
	            retorno.mensagemValidacao.push(item);
            });   
            callback(retorno);
        }
        else
        {
            var serviceEmpresa = new service.empresaService.Create();
            serviceEmpresa.save(e, function(err, ret){
                var a = new dto.responseDto.Create();
                if(err != null){
                    a.resposta = "Objeto inválido";
                    a.data = ret.data;
	                a.mensagemValidacao.push(JSON.stringify(err));
                }
                else
                {
                    a.resposta = "OK";
                    a.data = ret.data;
                }
                callback(a)
            });
        };
    };
};

    //     var validatorCliente = new validator.clienteValidator.Create();
    //     var retorno = validatorCliente.validate(c);
    //     if(!retorno.isValid){
    //         retorno.data = null;
    //         callback(retorno);
    //         return;
    //     }


    //     var cliente = config.getConnection().model('Cliente', repository.clienteModel.ClienteModel);
    //     cliente.findOne({ cpf: cpf}, function(err, ret){
    //         if(err){
    //             retorno.data = null;
    //             callback(retorno)
    //         }else{
    //             if(ret != null){
    //                 ret.pontos = ret.pontos + c.pontos;
    //                 ret.save();
    //             }
    //             retorno.data = ret;
    //             callback(retorno);
    //         }
    //     });
    // };
