/*
    repository/lib/clienteModel.js
*/

var config = require('../../config');

exports.ClienteModel = config.getConnection().model('Cliente', {
    nome: String,
    cpf: { type: String },
    celular: String,
    email: String,
    pontos: Number
});
