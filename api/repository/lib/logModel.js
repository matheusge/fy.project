/*
    repository/lib/logModel.js
*/

var config = require('../../config');

exports.LogModel = config.getConnection().model('Log', {
    descricao: String,
    modelo: String,
    erro: String,
    data: Date
});