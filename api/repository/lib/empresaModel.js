/*
    repository/lib/empresaModel.js
*/

var config = require('../../config');

 var m = config.getConnection().model('Empresa', {
    nome: String,
    cnpj: { type: String },
    config_pontos: Number,
    usuarios: [{
                    nome: String,
                    email: { type: String },
                    password: String
                }],
    clientes: [{
                    nome: String,
                    cpf: { type: String },
                    celular: String,
                    email: String,
                    pontos: Number
                }],
        logs: [{
                    descricao: String,
                    modelo: String,
                    erro: String,
                    data: Date
                }]
});

m.prototype.Compare = function(e){
    if(this.nome != e.nome){ this.nome = e.nome; };
    if(this.cnpj != e.cnpj){ this.cnpj = e.cnpj; };
    if(this.config_pontos != e.config_pontos){ this.config_pontos = e.config_pontos; };
};

exports.EmpresaModel = m;