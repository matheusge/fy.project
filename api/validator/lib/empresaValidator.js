/*
    validator/lib/empresaValidator.js
*/

var validatorResult = require('./validatorResult.js');
var _ = require('underscore');

exports.Create = function()
{
    this.validate = function(o){
        var retorno = new validatorResult.Create();
        if(o.nome == undefined || o.nome == ""){
            retorno.mensagemValidacao.push("É obrigatório informar o Nome");
            retorno.isValid = false;
        }
        if(o.cnpj == undefined || o.cnpj == ""){
            retorno.mensagemValidacao.push("É obrigatório informar o CNPJ");
            retorno.isValid = false;
        }
        if(o.config_pontos == undefined || o.config_pontos == ""){
            retorno.mensagemValidacao.push("É obrigatório informar a Configuração de Pontos");
            retorno.isValid = false;
        }

        if(_.countBy(o.usuarios, countUsuario)){
            var u = _.first(o.usuarios);
            //Implementar validator usuario;
        }else{
            retorno.mensagemValidacao.push("É obrigatório informar um Usuário");
            retorno.isValid = false;
        }

        return retorno;
    }

    var countUsuario = function(n){
        if(n >= 1){
            return true;
        }else{
            return false;
        }
    };
};