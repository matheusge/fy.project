/*
    config/lib/config.js
*/

var _config = require('../config.json');
var mongoose = require('mongoose');

Configuracao = function(){
    this.mongoConnection = _config.connectionMDB;
    this._conexao = null;

    this.createConnection = function(){
        var self = this;

        mongoose.connect(_config.connectionMDB, function(error){
            if(error) console.log(error);
            console.log("Conexão feita com sucesso!");
        });

        self._conexao = mongoose;
    };

    this.getConnection = function(){
        var self = this;
        if(self._conexao == null){
            self.createConnection();
        }

        return self._conexao;
    };
};

var config = module.exports = exports = new Configuracao();