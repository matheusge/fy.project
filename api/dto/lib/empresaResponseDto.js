/*
    dto/lib/empresaResponseDto.js
*/

exports.Create = function(){
    this.id = null;
    this.nome = null;
    this.cnpj = null;
    this.config_pontos = null;
    this.usuarios = [],
    this.clientes = [],
    this.logs = []
};