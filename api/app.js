var config = require('./config');
var express = require('express');
var app = express();
var controller = require('./controller');

/*
    To get POST parameters, we’ll need two the ExpressJS body-parser package. 
    This will allow us to grab information from the POST.
*/
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');
app.use(express.static(__dirname + './../public'));

app.use(function(req, res, next){
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();
});

//[####### EMPRESA-API #######]

app.get('/api/empresa/getall', function(red, res, next){
	res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    var controllerEmpresa = new controller.empresaController.Create();
    var ret = controllerEmpresa.getAll(function(ret){
        res.end(JSON.stringify(ret));
    });
});

app.post('/api/empresa/create', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    
    var emp = req.body;
    
    var controllerEmpresa = new controller.empresaController.Create();
    controllerEmpresa.create(emp, function(ret){
        res.end(JSON.stringify(ret));
    });
});

app.post('/api/empresa/remove', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    var id = req.body.id;
    var controllerEmpresa = new controller.empresaController.Create();
    controllerEmpresa.remove(id, function(ret){
        res.end(JSON.stringify(ret));
    });
});

app.post('/api/empresa/update', function(req, res, next){
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    var param = req.body;
    var e = {
        id: param.id,
        config_pontos: param.config_pontos,
        cnpj: param.cnpj,
        nome: param.nome,
    };
  
    var controllerEmpresa = new controller.empresaController.Create();
     controllerEmpresa.update(e, function(ret){
	      res.end(JSON.stringify(ret));
    });
});

//[####### EMPRESA-API #######]

//[####### EMPRESA-ADM #######]

app.get('/empresa/adm/create', function(req, res, next){
    res.render('pages/empresa/adm/create');
});

app.get('/empresa/adm/list', function(req, res, next){
    res.render('pages/empresa/adm/list');
});


app.get('/empresa/edit', function(req, res, next){
    // var _id = req.param('id'); //pegar id em cache
    var _id = "5834f72ed081ce682708277a"

    var controllerEmpresa = new controller.empresaController.Create();
    controllerEmpresa.getById(_id, function(ret){
	    //  console.log(ret);
         res.render('pages/empresa/update', { e : ret });
    });


    // if(_id == undefined){
    //     res.render('pages/empresa/adm/list'); //redirecionar rota
    // }
    // else{
         
    // }
});

//[/####### EMPRESA-ADM #######]

app.listen(8080, function(){
    console.log('Server up! 8080 é a porta mágica');
})