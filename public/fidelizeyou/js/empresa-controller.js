angular.module('appFY', ['ui.mask','ngAlertify'])
 .controller('EmpresaCtrl', ['$scope', '$http', 'alertify', function($scope, $http, alertify) {

   $scope.empresas = [];

   $scope.getAll = function () {
    $http.get('/api/empresa/getall').success(function(l) {
        $scope.empresas = l.data;
     });
   };

   $scope.submitCreate = function(){
     var e = {
      nome: $scope.e_nome,
      cnpj: $scope.e_cnpj,
      configpontos: $scope.e_configpontos,
      nomeusu: $scope.e_nomeusu,
      emailusu: $scope.e_emailusu,
      passusu: $scope.e_password
     };
 
     $http.post('/api/empresa/create', e).then(function(ret){
       if(ret.status == 200){
         alert("Empresa cadastrada com sucesso!");
         location.reload();
       }
     });
   };

   $scope.remove = function(data){
      var e = { id: data };

      var r = confirm("Confirmar exclusão da empresa?");
        if (r == true) {
          $http.post('/api/empresa/remove', e).then(function(ret){
              if(ret.status == 200){
                alert("Empresa excluída com sucesso!");
                location.reload();
              }else{
                alert("Erro: " + ret);
              };
            });
        }
   };

   $scope.fillFormUpdate = function(e){
     console.log('teste');
      // $("#id").val(e._id);
      // $('#empresaNome').val(e.nome);
      // $('#empresaCNPJ').val(e.cnpj);
      // $('#empresaConfigPontos').val(e.config_pontos);
   };

   $scope.update = function() {

     console.log('teste');

      // var e = {
      //   id : $("#id").val(),
      //   nome : $('#empresaNome').val(),
      //   cnpj : $('#empresaCNPJ').val(),
      //   configpontos : $('#empresaConfigPontos').val()
      // };

      // $http.post('/api/empresa/update', e).then(function(ret){
      //   if(ret.status == 200){
      //     alert("Empresa atualizada com sucesso!");
      //     location.reload();
      //   }
      // });
   };  

   $scope.updateSubmit = function(){
      var e = {
        nome: $("#empresaNome").val(),
        cnpj: $("#empresaCNPJ").val(),
        config_pontos: $("#empresaConfigPontos").val()
      };
      
      $http.post('/api/empresa/update', e).then(function(ret){
      if(ret.status == 200){
          var r = ret.data;

          if(r.isValida != undefined && !r.isValid){
            angular.forEach(r.mensagemValidacao, function(value, key){
              alertify.error(value);
            });
          }else{
            angular.forEach(r.mensagemValidacao, function(value, key){
              alertify.success("Empresa atualizada com sucesso");;
            });
          };
        }
     });

    }; 
   
 }]);
 